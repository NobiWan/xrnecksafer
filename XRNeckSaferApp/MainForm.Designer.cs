﻿namespace XRNeckSafer
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer _components = null;

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem1 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroup actionPropertyGroup1 = new XRNeckSafer.ActionPropertyGroup();
            XRNeckSafer.ActionPropertyGroup actionPropertyGroup2 = new XRNeckSafer.ActionPropertyGroup();
            XRNeckSafer.ActionPropertyGroup actionPropertyGroup3 = new XRNeckSafer.ActionPropertyGroup();
            XRNeckSafer.ActionPropertyGroup actionPropertyGroup4 = new XRNeckSafer.ActionPropertyGroup();
            XRNeckSafer.ActionPropertyGroup actionPropertyGroup5 = new XRNeckSafer.ActionPropertyGroup();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem2 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem3 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem4 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem12 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem6 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem7 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem8 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem9 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem10 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem11 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem13 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem14 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem15 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem16 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem17 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem18 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem19 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem20 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem21 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem22 = new XRNeckSafer.ActionPropertyGroupItem();
            XRNeckSafer.ActionPropertyGroupItem actionPropertyGroupItem5 = new XRNeckSafer.ActionPropertyGroupItem();
            this.stepwiseGroup = new System.Windows.Forms.GroupBox();
            this.error_label2 = new System.Windows.Forms.Label();
            this.error_label = new System.Windows.Forms.Label();
            this.graphButton = new System.Windows.Forms.Button();
            this.AutorotGridView = new System.Windows.Forms.DataGridView();
            this.act = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deact = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fwd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.linearGroup = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._numericUpDownMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.setJoystickkeyShortcutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.HMDYawBox = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.HMDYawLabel = new System.Windows.Forms.Label();
            this.VersionLabel = new System.Windows.Forms.Label();
            this.ManualGroup = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.snapRB = new System.Windows.Forms.RadioButton();
            this.additivRB = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.RightLabel = new System.Windows.Forms.Label();
            this.LeftLabel = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.advancedConfigToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startMinimzedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizeToTrayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.PitchLimToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this._disableSplashScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableAllGUIOutputToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.showJoystickkeyConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showJoystickkeyboardMappsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.resetOptionsToDefaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPILayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listApiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableAPILayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ARGroup = new System.Windows.Forms.GroupBox();
            this.ARlinear = new System.Windows.Forms.RadioButton();
            this.ARstepwise = new System.Windows.Forms.RadioButton();
            this.AROffButton = new System.Windows.Forms.RadioButton();
            this.YawPitchTab = new System.Windows.Forms.TabControl();
            this.YawTab = new System.Windows.Forms.TabPage();
            this.PitchTab = new System.Windows.Forms.TabPage();
            this.pARGroup = new System.Windows.Forms.GroupBox();
            this.pARlinear = new System.Windows.Forms.RadioButton();
            this.pARstepwise = new System.Windows.Forms.RadioButton();
            this.pLinearGroup = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.pAROffButton = new System.Windows.Forms.RadioButton();
            this.pStepwiseGroup = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.downErrorLabel2 = new System.Windows.Forms.Label();
            this.downErrorLabel1 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.DownAddButton = new System.Windows.Forms.Button();
            this.DownDelButton = new System.Windows.Forms.Button();
            this.DownAutorotGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.upErrorLabel2 = new System.Windows.Forms.Label();
            this.upErrorLabel1 = new System.Windows.Forms.Label();
            this.UpAutorotGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpAddButton = new System.Windows.Forms.Button();
            this.UpDelButton = new System.Windows.Forms.Button();
            this.pManualGroup = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.pSnapRP = new System.Windows.Forms.RadioButton();
            this.pAdditivRB = new System.Windows.Forms.RadioButton();
            this.label31 = new System.Windows.Forms.Label();
            this.DownLabel = new System.Windows.Forms.Label();
            this.UpLabel = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.loopTimer = new System.Windows.Forms.Timer(this.components);
            this._devicesStatusLabel = new System.Windows.Forms.Label();
            this._devicesStatusImage = new System.Windows.Forms.PictureBox();
            this.numericUpDownMultRight = new XRNeckSafer.NumericActionUpDown();
            this._groups = new XRNeckSafer.ActionPropertyGroups();
            this.numericUpDownMultLeft = new XRNeckSafer.NumericActionUpDown();
            this.numericUpDownStartRight = new XRNeckSafer.NumericActionUpDown();
            this.numericUpDownStartLeft = new XRNeckSafer.NumericActionUpDown();
            this.YawAutorotationHoldButton = new XRNeckSafer.BooleanActionButton();
            this.AccumReset = new XRNeckSafer.BooleanActionButton();
            this.transFNUP = new XRNeckSafer.NumericActionUpDown();
            this.angleNUD = new XRNeckSafer.NumericActionUpDown();
            this.transLRNUP = new XRNeckSafer.NumericActionUpDown();
            this.SetLeftButton = new XRNeckSafer.BooleanActionButton();
            this.SetRightButton = new XRNeckSafer.BooleanActionButton();
            this.numericUpDownMultDown = new XRNeckSafer.NumericActionUpDown();
            this.numericUpDownMultUp = new XRNeckSafer.NumericActionUpDown();
            this.numericUpDownStartDown = new XRNeckSafer.NumericActionUpDown();
            this.numericUpDownStartUp = new XRNeckSafer.NumericActionUpDown();
            this.PitchAutorotationHoldButton = new XRNeckSafer.BooleanActionButton();
            this.pAccumReset = new XRNeckSafer.BooleanActionButton();
            this.upNUD = new XRNeckSafer.NumericActionUpDown();
            this.downNUD = new XRNeckSafer.NumericActionUpDown();
            this.SetUpButton = new XRNeckSafer.BooleanActionButton();
            this.SetDownButton = new XRNeckSafer.BooleanActionButton();
            this.SetResetButton = new XRNeckSafer.BooleanActionButton();
            this.stepwiseGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutorotGridView)).BeginInit();
            this.linearGroup.SuspendLayout();
            this._numericUpDownMenuStrip.SuspendLayout();
            this.HMDYawBox.SuspendLayout();
            this.ManualGroup.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.ARGroup.SuspendLayout();
            this.YawPitchTab.SuspendLayout();
            this.YawTab.SuspendLayout();
            this.PitchTab.SuspendLayout();
            this.pARGroup.SuspendLayout();
            this.pLinearGroup.SuspendLayout();
            this.pStepwiseGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DownAutorotGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpAutorotGridView)).BeginInit();
            this.pManualGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._devicesStatusImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transFNUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.angleNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transLRNUP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.downNUD)).BeginInit();
            this.SuspendLayout();
            // 
            // stepwiseGroup
            // 
            this.stepwiseGroup.Controls.Add(this.error_label2);
            this.stepwiseGroup.Controls.Add(this.error_label);
            this.stepwiseGroup.Controls.Add(this.graphButton);
            this.stepwiseGroup.Controls.Add(this.AutorotGridView);
            this.stepwiseGroup.Controls.Add(this.AddButton);
            this.stepwiseGroup.Controls.Add(this.DeleteButton);
            this.stepwiseGroup.Location = new System.Drawing.Point(10, 40);
            this.stepwiseGroup.Name = "stepwiseGroup";
            this.stepwiseGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.stepwiseGroup.Size = new System.Drawing.Size(235, 166);
            this.stepwiseGroup.TabIndex = 13;
            this.stepwiseGroup.TabStop = false;
            // 
            // error_label2
            // 
            this.error_label2.AutoSize = true;
            this.error_label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_label2.ForeColor = System.Drawing.Color.Red;
            this.error_label2.Location = new System.Drawing.Point(129, 29);
            this.error_label2.Name = "error_label2";
            this.error_label2.Size = new System.Drawing.Size(38, 13);
            this.error_label2.TabIndex = 57;
            this.error_label2.Text = "value";
            // 
            // error_label
            // 
            this.error_label.AutoSize = true;
            this.error_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_label.ForeColor = System.Drawing.Color.Red;
            this.error_label.Location = new System.Drawing.Point(126, 18);
            this.error_label.Name = "error_label";
            this.error_label.Size = new System.Drawing.Size(45, 13);
            this.error_label.TabIndex = 56;
            this.error_label.Text = "Invalid";
            // 
            // graphButton
            // 
            this.graphButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("graphButton.BackgroundImage")));
            this.graphButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.graphButton.Location = new System.Drawing.Point(176, 18);
            this.graphButton.Name = "graphButton";
            this.graphButton.Size = new System.Drawing.Size(24, 22);
            this.graphButton.TabIndex = 55;
            this.graphButton.UseVisualStyleBackColor = true;
            this.graphButton.Click += new System.EventHandler(this.graphButton_Click);
            // 
            // AutorotGridView
            // 
            this.AutorotGridView.AllowUserToAddRows = false;
            this.AutorotGridView.AllowUserToDeleteRows = false;
            this.AutorotGridView.AllowUserToResizeColumns = false;
            this.AutorotGridView.AllowUserToResizeRows = false;
            this.AutorotGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AutorotGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.act,
            this.deact,
            this.rot,
            this.LR,
            this.Fwd});
            this.AutorotGridView.Location = new System.Drawing.Point(31, 43);
            this.AutorotGridView.Name = "AutorotGridView";
            this.AutorotGridView.RowHeadersWidth = 51;
            this.AutorotGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.AutorotGridView.Size = new System.Drawing.Size(169, 114);
            this.AutorotGridView.TabIndex = 39;
            this.AutorotGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.AutorotGridView_CellValueChanged);
            this.AutorotGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.AutorotGridView_RowsAdded);
            this.AutorotGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.AutorotGridView_RowsRemoved);
            // 
            // act
            // 
            this.act.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.act.DefaultCellStyle = dataGridViewCellStyle1;
            this.act.Frozen = true;
            this.act.HeaderText = "act";
            this.act.MinimumWidth = 6;
            this.act.Name = "act";
            this.act.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.act.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.act.ToolTipText = "Activation angle";
            this.act.Width = 30;
            // 
            // deact
            // 
            this.deact.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deact.DefaultCellStyle = dataGridViewCellStyle2;
            this.deact.Frozen = true;
            this.deact.HeaderText = "deact";
            this.deact.MinimumWidth = 6;
            this.deact.Name = "deact";
            this.deact.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.deact.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.deact.ToolTipText = "Deactivation angle (< act and > previous act)";
            this.deact.Width = 30;
            // 
            // rot
            // 
            this.rot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rot.DefaultCellStyle = dataGridViewCellStyle3;
            this.rot.Frozen = true;
            this.rot.HeaderText = "rot";
            this.rot.MinimumWidth = 6;
            this.rot.Name = "rot";
            this.rot.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.rot.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.rot.ToolTipText = "Rotation angle";
            this.rot.Width = 30;
            // 
            // LR
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LR.DefaultCellStyle = dataGridViewCellStyle4;
            this.LR.Frozen = true;
            this.LR.HeaderText = "L/R";
            this.LR.MinimumWidth = 6;
            this.LR.Name = "LR";
            this.LR.ToolTipText = "Translation Left/Right (<40cm)";
            this.LR.Width = 30;
            // 
            // Fwd
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fwd.DefaultCellStyle = dataGridViewCellStyle5;
            this.Fwd.Frozen = true;
            this.Fwd.HeaderText = "Fwd";
            this.Fwd.MinimumWidth = 6;
            this.Fwd.Name = "Fwd";
            this.Fwd.ToolTipText = "Translation to front (< 20cm)";
            this.Fwd.Width = 50;
            // 
            // AddButton
            // 
            this.AddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddButton.Location = new System.Drawing.Point(31, 18);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(43, 22);
            this.AddButton.TabIndex = 40;
            this.AddButton.Text = "add";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeleteButton.Location = new System.Drawing.Point(78, 18);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(43, 22);
            this.DeleteButton.TabIndex = 41;
            this.DeleteButton.Text = "del";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // linearGroup
            // 
            this.linearGroup.Controls.Add(this.label20);
            this.linearGroup.Controls.Add(this.label18);
            this.linearGroup.Controls.Add(this.label13);
            this.linearGroup.Controls.Add(this.label7);
            this.linearGroup.Controls.Add(this.label6);
            this.linearGroup.Controls.Add(this.label5);
            this.linearGroup.Controls.Add(this.label4);
            this.linearGroup.Controls.Add(this.label2);
            this.linearGroup.Controls.Add(this.numericUpDownMultRight);
            this.linearGroup.Controls.Add(this.numericUpDownMultLeft);
            this.linearGroup.Controls.Add(this.numericUpDownStartRight);
            this.linearGroup.Controls.Add(this.numericUpDownStartLeft);
            this.linearGroup.Location = new System.Drawing.Point(8, 97);
            this.linearGroup.Name = "linearGroup";
            this.linearGroup.Size = new System.Drawing.Size(225, 89);
            this.linearGroup.TabIndex = 57;
            this.linearGroup.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(137, 13);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 15);
            this.label20.TabIndex = 58;
            this.label20.Text = "Amplify by";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(43, 13);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 15);
            this.label18.TabIndex = 57;
            this.label18.Text = "Start at";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(194, 43);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 15);
            this.label13.TabIndex = 56;
            this.label13.Text = "%";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(93, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 15);
            this.label7.TabIndex = 55;
            this.label7.Text = "deg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(127, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "R";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(28, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "R";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(128, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "L";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "L";
            // 
            // _numericUpDownMenuStrip
            // 
            this._numericUpDownMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setJoystickkeyShortcutToolStripMenuItem});
            this._numericUpDownMenuStrip.Name = "_numericUpDownMenuStrip";
            this._numericUpDownMenuStrip.Size = new System.Drawing.Size(235, 26);
            // 
            // setJoystickkeyShortcutToolStripMenuItem
            // 
            this.setJoystickkeyShortcutToolStripMenuItem.Name = "setJoystickkeyShortcutToolStripMenuItem";
            this.setJoystickkeyShortcutToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.setJoystickkeyShortcutToolStripMenuItem.Text = "Set joystick/keyboard shortcut";
            this.setJoystickkeyShortcutToolStripMenuItem.Click += new System.EventHandler(this.OnSetJoystickKeyShortcutMenuClick);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 33);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(239, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Set Center Button to the same button as in game:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "-  IL-2:   ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "-  DCS:  ";
            // 
            // HMDYawBox
            // 
            this.HMDYawBox.Controls.Add(this.label42);
            this.HMDYawBox.Controls.Add(this.label41);
            this.HMDYawBox.Controls.Add(this.label40);
            this.HMDYawBox.Controls.Add(this.label33);
            this.HMDYawBox.Controls.Add(this.label1);
            this.HMDYawBox.Controls.Add(this.label3);
            this.HMDYawBox.Controls.Add(this.SetResetButton);
            this.HMDYawBox.Controls.Add(this.HMDYawLabel);
            this.HMDYawBox.Controls.Add(this.label11);
            this.HMDYawBox.Controls.Add(this.label12);
            this.HMDYawBox.Controls.Add(this.label10);
            this.HMDYawBox.Location = new System.Drawing.Point(12, 25);
            this.HMDYawBox.Name = "HMDYawBox";
            this.HMDYawBox.Size = new System.Drawing.Size(256, 113);
            this.HMDYawBox.TabIndex = 31;
            this.HMDYawBox.TabStop = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(53, 79);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(125, 13);
            this.label42.TabIndex = 59;
            this.label42.Text = "\"VR - CAMERA RESET\"";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(53, 64);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(117, 13);
            this.label41.TabIndex = 58;
            this.label41.Text = "\"recenter VR Headset\"";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(53, 49);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(101, 13);
            this.label40.TabIndex = 57;
            this.label40.Text = "\"Default VR View\"  ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(8, 79);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(51, 13);
            this.label33.TabIndex = 56;
            this.label33.Text = "-  MSFS: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 13);
            this.label1.TabIndex = 55;
            this.label1.Text = "When in cockpit press Center Button to calibrate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, -2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 16);
            this.label3.TabIndex = 54;
            this.label3.Text = "Calibration";
            // 
            // HMDYawLabel
            // 
            this.HMDYawLabel.AutoSize = true;
            this.HMDYawLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HMDYawLabel.Location = new System.Drawing.Point(67, 18);
            this.HMDYawLabel.Name = "HMDYawLabel";
            this.HMDYawLabel.Size = new System.Drawing.Size(101, 13);
            this.HMDYawLabel.TabIndex = 27;
            this.HMDYawLabel.Text = "HMD yaw: 0 deg";
            this.toolTip1.SetToolTip(this.HMDYawLabel, "Physical rotation angle of the headset");
            // 
            // VersionLabel
            // 
            this.VersionLabel.AutoSize = true;
            this.VersionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VersionLabel.Location = new System.Drawing.Point(234, 829);
            this.VersionLabel.Name = "VersionLabel";
            this.VersionLabel.Size = new System.Drawing.Size(34, 13);
            this.VersionLabel.TabIndex = 34;
            this.VersionLabel.Text = "beta3";
            this.VersionLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ManualGroup
            // 
            this.ManualGroup.Controls.Add(this.AccumReset);
            this.ManualGroup.Controls.Add(this.label25);
            this.ManualGroup.Controls.Add(this.transFNUP);
            this.ManualGroup.Controls.Add(this.label24);
            this.ManualGroup.Controls.Add(this.angleNUD);
            this.ManualGroup.Controls.Add(this.transLRNUP);
            this.ManualGroup.Controls.Add(this.label8);
            this.ManualGroup.Controls.Add(this.label17);
            this.ManualGroup.Controls.Add(this.label14);
            this.ManualGroup.Controls.Add(this.label23);
            this.ManualGroup.Controls.Add(this.snapRB);
            this.ManualGroup.Controls.Add(this.additivRB);
            this.ManualGroup.Controls.Add(this.label16);
            this.ManualGroup.Controls.Add(this.label9);
            this.ManualGroup.Controls.Add(this.label15);
            this.ManualGroup.Controls.Add(this.SetLeftButton);
            this.ManualGroup.Controls.Add(this.SetRightButton);
            this.ManualGroup.Controls.Add(this.RightLabel);
            this.ManualGroup.Controls.Add(this.LeftLabel);
            this.ManualGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ManualGroup.Location = new System.Drawing.Point(4, 3);
            this.ManualGroup.Name = "ManualGroup";
            this.ManualGroup.Size = new System.Drawing.Size(252, 138);
            this.ManualGroup.TabIndex = 38;
            this.ManualGroup.TabStop = false;
            this.ManualGroup.Text = "Manual Rotation";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(7, 85);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 15);
            this.label25.TabIndex = 53;
            this.label25.Text = "Mode";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(139, 46);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 15);
            this.label24.TabIndex = 52;
            this.label24.Text = "Translation";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(72, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "deg";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(223, 88);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(21, 13);
            this.label17.TabIndex = 30;
            this.label17.Text = "cm";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(143, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "L/R";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(7, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 15);
            this.label23.TabIndex = 51;
            this.label23.Text = "Rotation";
            // 
            // snapRB
            // 
            this.snapRB.AutoSize = true;
            this.snapRB.Checked = true;
            this.snapRB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.snapRB.Location = new System.Drawing.Point(12, 101);
            this.snapRB.Name = "snapRB";
            this.snapRB.Size = new System.Drawing.Size(50, 17);
            this.snapRB.TabIndex = 1;
            this.snapRB.TabStop = true;
            this.snapRB.Text = "Snap";
            this.snapRB.UseVisualStyleBackColor = true;
            // 
            // additivRB
            // 
            this.additivRB.AutoSize = true;
            this.additivRB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.additivRB.Location = new System.Drawing.Point(12, 116);
            this.additivRB.Name = "additivRB";
            this.additivRB.Size = new System.Drawing.Size(58, 17);
            this.additivRB.TabIndex = 12;
            this.additivRB.Text = "Accum";
            this.additivRB.UseVisualStyleBackColor = true;
            this.additivRB.CheckedChanged += new System.EventHandler(this.additivRB_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(143, 88);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Fwd.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 67);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "+/-";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(223, 66);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(21, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "cm";
            // 
            // RightLabel
            // 
            this.RightLabel.AutoSize = true;
            this.RightLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RightLabel.Location = new System.Drawing.Point(134, 22);
            this.RightLabel.Margin = new System.Windows.Forms.Padding(0);
            this.RightLabel.Name = "RightLabel";
            this.RightLabel.Size = new System.Drawing.Size(35, 22);
            this.RightLabel.TabIndex = 39;
            this.RightLabel.Text = "R :";
            // 
            // LeftLabel
            // 
            this.LeftLabel.AutoSize = true;
            this.LeftLabel.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeftLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LeftLabel.Location = new System.Drawing.Point(4, 21);
            this.LeftLabel.Name = "LeftLabel";
            this.LeftLabel.Size = new System.Drawing.Size(36, 24);
            this.LeftLabel.TabIndex = 38;
            this.LeftLabel.Text = "L :";
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.advancedConfigToolStripMenuItem,
            this.aPILayerToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(276, 24);
            this.menuStrip.TabIndex = 55;
            this.menuStrip.Text = "menuStrip";
            // 
            // advancedConfigToolStripMenuItem
            // 
            this.advancedConfigToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.advancedConfigToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startMinimzedToolStripMenuItem,
            this.minimizeToTrayToolStripMenuItem,
            this.toolStripSeparator1,
            this.PitchLimToolStripMenuItem,
            this.toolStripSeparator3,
            this._disableSplashScreenToolStripMenuItem,
            this.disableAllGUIOutputToolStripMenuItem,
            this.toolStripSeparator2,
            this.showJoystickkeyConfigurationToolStripMenuItem,
            this.showJoystickkeyboardMappsToolStripMenuItem,
            this.toolStripSeparator4,
            this.resetOptionsToDefaultToolStripMenuItem});
            this.advancedConfigToolStripMenuItem.Name = "advancedConfigToolStripMenuItem";
            this.advancedConfigToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.advancedConfigToolStripMenuItem.Text = "Options";
            // 
            // startMinimzedToolStripMenuItem
            // 
            this.startMinimzedToolStripMenuItem.CheckOnClick = true;
            this.startMinimzedToolStripMenuItem.Name = "startMinimzedToolStripMenuItem";
            this.startMinimzedToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.startMinimzedToolStripMenuItem.Text = "Start minimzed";
            this.startMinimzedToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.startMinimzedToolStripMenuItem_CheckStateChanged);
            // 
            // minimizeToTrayToolStripMenuItem
            // 
            this.minimizeToTrayToolStripMenuItem.CheckOnClick = true;
            this.minimizeToTrayToolStripMenuItem.Name = "minimizeToTrayToolStripMenuItem";
            this.minimizeToTrayToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.minimizeToTrayToolStripMenuItem.Text = "Minimize to tray";
            this.minimizeToTrayToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.minimizeToTrayToolStripMenuItem_CheckStateChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(259, 6);
            // 
            // PitchLimToolStripMenuItem
            // 
            this.PitchLimToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10,
            this.toolStripMenuItem11});
            this.PitchLimToolStripMenuItem.Name = "PitchLimToolStripMenuItem";
            this.PitchLimToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.PitchLimToolStripMenuItem.Text = "Pitch limit for Autorot";
            this.PitchLimToolStripMenuItem.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.PitchLimToolStripMenuItem_DropDownItemClicked);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem2.Text = "10 deg";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem4.Text = "20 deg";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem5.Text = "30 deg";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem6.Text = "40 deg";
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem7.Text = "50 deg";
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem8.Text = "60 deg";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem9.Text = "70 deg";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem10.Text = "80 deg";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem11.Text = "90 deg";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(259, 6);
            // 
            // _disableSplashScreenToolStripMenuItem
            // 
            this._disableSplashScreenToolStripMenuItem.CheckOnClick = true;
            this._disableSplashScreenToolStripMenuItem.Name = "_disableSplashScreenToolStripMenuItem";
            this._disableSplashScreenToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this._disableSplashScreenToolStripMenuItem.Text = "Disable Splash Screen";
            this._disableSplashScreenToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.OnDisableSplashScreenCheckStateChanged);
            // 
            // disableAllGUIOutputToolStripMenuItem
            // 
            this.disableAllGUIOutputToolStripMenuItem.CheckOnClick = true;
            this.disableAllGUIOutputToolStripMenuItem.Name = "disableAllGUIOutputToolStripMenuItem";
            this.disableAllGUIOutputToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.disableAllGUIOutputToolStripMenuItem.Text = "Disable angle output in GUI";
            this.disableAllGUIOutputToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.disableAllGUIOutputToolStripMenuItem_CheckStateChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(259, 6);
            // 
            // showJoystickkeyConfigurationToolStripMenuItem
            // 
            this.showJoystickkeyConfigurationToolStripMenuItem.Name = "showJoystickkeyConfigurationToolStripMenuItem";
            this.showJoystickkeyConfigurationToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.showJoystickkeyConfigurationToolStripMenuItem.Text = "Edit joystick/key mapping";
            this.showJoystickkeyConfigurationToolStripMenuItem.Click += new System.EventHandler(this.OnShowJoystickKeyConfigMenuClick);
            // 
            // showJoystickkeyboardMappsToolStripMenuItem
            // 
            this.showJoystickkeyboardMappsToolStripMenuItem.Name = "showJoystickkeyboardMappsToolStripMenuItem";
            this.showJoystickkeyboardMappsToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.showJoystickkeyboardMappsToolStripMenuItem.Text = "Assign keyboard buttons to joystick";
            this.showJoystickkeyboardMappsToolStripMenuItem.Click += new System.EventHandler(this.OnShowJoyKeyboardMapsClick);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(259, 6);
            // 
            // resetOptionsToDefaultToolStripMenuItem
            // 
            this.resetOptionsToDefaultToolStripMenuItem.Name = "resetOptionsToDefaultToolStripMenuItem";
            this.resetOptionsToDefaultToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.resetOptionsToDefaultToolStripMenuItem.Text = "Reset options to default";
            this.resetOptionsToDefaultToolStripMenuItem.Click += new System.EventHandler(this.resetOptionsToDefaultToolStripMenuItem_Click);
            // 
            // aPILayerToolStripMenuItem
            // 
            this.aPILayerToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.aPILayerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listApiToolStripMenuItem,
            this.disableAPILayerToolStripMenuItem});
            this.aPILayerToolStripMenuItem.Name = "aPILayerToolStripMenuItem";
            this.aPILayerToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.aPILayerToolStripMenuItem.Text = "OpenXR";
            // 
            // listApiToolStripMenuItem
            // 
            this.listApiToolStripMenuItem.Name = "listApiToolStripMenuItem";
            this.listApiToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.listApiToolStripMenuItem.Text = "Show active OpenXR API Layers";
            this.listApiToolStripMenuItem.Click += new System.EventHandler(this.listApiToolStripMenuItem_Click);
            // 
            // disableAPILayerToolStripMenuItem
            // 
            this.disableAPILayerToolStripMenuItem.CheckOnClick = true;
            this.disableAPILayerToolStripMenuItem.Name = "disableAPILayerToolStripMenuItem";
            this.disableAPILayerToolStripMenuItem.Size = new System.Drawing.Size(259, 22);
            this.disableAPILayerToolStripMenuItem.Text = "Deactivate XRNS OpenXR API Layer";
            this.disableAPILayerToolStripMenuItem.Click += new System.EventHandler(this.OnDisableAPILayerToolStripMenuItemClicked);
            // 
            // notifyIcon
            // 
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "XRNeckSafer";
            this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(104, 48);
            // 
            // showToolStripMenuItem
            // 
            this.showToolStripMenuItem.Name = "showToolStripMenuItem";
            this.showToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.showToolStripMenuItem.Text = "Show";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // ARGroup
            // 
            this.ARGroup.Controls.Add(this.linearGroup);
            this.ARGroup.Controls.Add(this.ARlinear);
            this.ARGroup.Controls.Add(this.ARstepwise);
            this.ARGroup.Controls.Add(this.AROffButton);
            this.ARGroup.Controls.Add(this.stepwiseGroup);
            this.ARGroup.Controls.Add(this.YawAutorotationHoldButton);
            this.ARGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ARGroup.Location = new System.Drawing.Point(5, 152);
            this.ARGroup.Name = "ARGroup";
            this.ARGroup.Size = new System.Drawing.Size(251, 126);
            this.ARGroup.TabIndex = 56;
            this.ARGroup.TabStop = false;
            this.ARGroup.Text = "Autorotation";
            // 
            // ARlinear
            // 
            this.ARlinear.AutoSize = true;
            this.ARlinear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ARlinear.Location = new System.Drawing.Point(84, 18);
            this.ARlinear.Name = "ARlinear";
            this.ARlinear.Size = new System.Drawing.Size(67, 17);
            this.ARlinear.TabIndex = 57;
            this.ARlinear.Text = "Smooth";
            this.ARlinear.UseVisualStyleBackColor = true;
            this.ARlinear.CheckedChanged += new System.EventHandler(this.autorot_changed);
            // 
            // ARstepwise
            // 
            this.ARstepwise.AutoSize = true;
            this.ARstepwise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ARstepwise.Location = new System.Drawing.Point(6, 18);
            this.ARstepwise.Name = "ARstepwise";
            this.ARstepwise.Size = new System.Drawing.Size(76, 17);
            this.ARstepwise.TabIndex = 56;
            this.ARstepwise.Text = "Stepwise";
            this.ARstepwise.UseVisualStyleBackColor = true;
            this.ARstepwise.CheckedChanged += new System.EventHandler(this.autorot_changed);
            // 
            // AROffButton
            // 
            this.AROffButton.AutoSize = true;
            this.AROffButton.Checked = true;
            this.AROffButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AROffButton.Location = new System.Drawing.Point(152, 18);
            this.AROffButton.Name = "AROffButton";
            this.AROffButton.Size = new System.Drawing.Size(42, 17);
            this.AROffButton.TabIndex = 55;
            this.AROffButton.TabStop = true;
            this.AROffButton.Text = "Off";
            this.AROffButton.UseVisualStyleBackColor = true;
            this.AROffButton.CheckedChanged += new System.EventHandler(this.autorot_changed);
            // 
            // YawPitchTab
            // 
            this.YawPitchTab.Controls.Add(this.YawTab);
            this.YawPitchTab.Controls.Add(this.PitchTab);
            this.YawPitchTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YawPitchTab.Location = new System.Drawing.Point(6, 140);
            this.YawPitchTab.Name = "YawPitchTab";
            this.YawPitchTab.SelectedIndex = 0;
            this.YawPitchTab.Size = new System.Drawing.Size(268, 470);
            this.YawPitchTab.TabIndex = 57;
            this.YawPitchTab.SelectedIndexChanged += new System.EventHandler(this.YawPitchTab_SelectedIndexChanged);
            // 
            // YawTab
            // 
            this.YawTab.BackColor = System.Drawing.Color.Transparent;
            this.YawTab.Controls.Add(this.ARGroup);
            this.YawTab.Controls.Add(this.ManualGroup);
            this.YawTab.Location = new System.Drawing.Point(4, 26);
            this.YawTab.Name = "YawTab";
            this.YawTab.Padding = new System.Windows.Forms.Padding(3);
            this.YawTab.Size = new System.Drawing.Size(260, 440);
            this.YawTab.TabIndex = 0;
            this.YawTab.Text = "Yaw";
            this.YawTab.UseVisualStyleBackColor = true;
            // 
            // PitchTab
            // 
            this.PitchTab.BackColor = System.Drawing.Color.Transparent;
            this.PitchTab.Controls.Add(this.pARGroup);
            this.PitchTab.Controls.Add(this.pManualGroup);
            this.PitchTab.Location = new System.Drawing.Point(4, 26);
            this.PitchTab.Name = "PitchTab";
            this.PitchTab.Padding = new System.Windows.Forms.Padding(3);
            this.PitchTab.Size = new System.Drawing.Size(260, 440);
            this.PitchTab.TabIndex = 1;
            this.PitchTab.Text = "Pitch";
            this.PitchTab.UseVisualStyleBackColor = true;
            // 
            // pARGroup
            // 
            this.pARGroup.Controls.Add(this.pARlinear);
            this.pARGroup.Controls.Add(this.pARstepwise);
            this.pARGroup.Controls.Add(this.pLinearGroup);
            this.pARGroup.Controls.Add(this.pAROffButton);
            this.pARGroup.Controls.Add(this.pStepwiseGroup);
            this.pARGroup.Controls.Add(this.PitchAutorotationHoldButton);
            this.pARGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pARGroup.Location = new System.Drawing.Point(5, 152);
            this.pARGroup.Name = "pARGroup";
            this.pARGroup.Size = new System.Drawing.Size(251, 181);
            this.pARGroup.TabIndex = 57;
            this.pARGroup.TabStop = false;
            this.pARGroup.Text = "Autorotation";
            // 
            // pARlinear
            // 
            this.pARlinear.AutoSize = true;
            this.pARlinear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pARlinear.Location = new System.Drawing.Point(84, 18);
            this.pARlinear.Name = "pARlinear";
            this.pARlinear.Size = new System.Drawing.Size(67, 17);
            this.pARlinear.TabIndex = 57;
            this.pARlinear.Text = "Smooth";
            this.pARlinear.UseVisualStyleBackColor = true;
            this.pARlinear.CheckedChanged += new System.EventHandler(this.pitchAutorotChanged);
            // 
            // pARstepwise
            // 
            this.pARstepwise.AutoSize = true;
            this.pARstepwise.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pARstepwise.Location = new System.Drawing.Point(6, 18);
            this.pARstepwise.Name = "pARstepwise";
            this.pARstepwise.Size = new System.Drawing.Size(76, 17);
            this.pARstepwise.TabIndex = 56;
            this.pARstepwise.Text = "Stepwise";
            this.pARstepwise.UseVisualStyleBackColor = true;
            this.pARstepwise.CheckedChanged += new System.EventHandler(this.pitchAutorotChanged);
            // 
            // pLinearGroup
            // 
            this.pLinearGroup.Controls.Add(this.label29);
            this.pLinearGroup.Controls.Add(this.label30);
            this.pLinearGroup.Controls.Add(this.label34);
            this.pLinearGroup.Controls.Add(this.label35);
            this.pLinearGroup.Controls.Add(this.label36);
            this.pLinearGroup.Controls.Add(this.label37);
            this.pLinearGroup.Controls.Add(this.label38);
            this.pLinearGroup.Controls.Add(this.label39);
            this.pLinearGroup.Controls.Add(this.numericUpDownMultDown);
            this.pLinearGroup.Controls.Add(this.numericUpDownMultUp);
            this.pLinearGroup.Controls.Add(this.numericUpDownStartDown);
            this.pLinearGroup.Controls.Add(this.numericUpDownStartUp);
            this.pLinearGroup.Location = new System.Drawing.Point(43, 40);
            this.pLinearGroup.Name = "pLinearGroup";
            this.pLinearGroup.Size = new System.Drawing.Size(225, 89);
            this.pLinearGroup.TabIndex = 57;
            this.pLinearGroup.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(137, 13);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(61, 15);
            this.label29.TabIndex = 58;
            this.label29.Text = "Amplify by";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(43, 13);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 15);
            this.label30.TabIndex = 57;
            this.label30.Text = "Start at";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(194, 43);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(18, 15);
            this.label34.TabIndex = 56;
            this.label34.Text = "%";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(93, 43);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(28, 15);
            this.label35.TabIndex = 55;
            this.label35.Text = "deg";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(127, 60);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(16, 13);
            this.label36.TabIndex = 33;
            this.label36.Text = "D";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(29, 60);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(16, 13);
            this.label37.TabIndex = 32;
            this.label37.Text = "D";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(127, 34);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(16, 13);
            this.label38.TabIndex = 31;
            this.label38.Text = "U";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(29, 34);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(16, 13);
            this.label39.TabIndex = 30;
            this.label39.Text = "U";
            // 
            // pAROffButton
            // 
            this.pAROffButton.AutoSize = true;
            this.pAROffButton.Checked = true;
            this.pAROffButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pAROffButton.Location = new System.Drawing.Point(152, 18);
            this.pAROffButton.Name = "pAROffButton";
            this.pAROffButton.Size = new System.Drawing.Size(42, 17);
            this.pAROffButton.TabIndex = 55;
            this.pAROffButton.TabStop = true;
            this.pAROffButton.Text = "Off";
            this.pAROffButton.UseVisualStyleBackColor = true;
            this.pAROffButton.CheckedChanged += new System.EventHandler(this.pitchAutorotChanged);
            // 
            // pStepwiseGroup
            // 
            this.pStepwiseGroup.Controls.Add(this.label43);
            this.pStepwiseGroup.Controls.Add(this.downErrorLabel2);
            this.pStepwiseGroup.Controls.Add(this.downErrorLabel1);
            this.pStepwiseGroup.Controls.Add(this.label32);
            this.pStepwiseGroup.Controls.Add(this.DownAddButton);
            this.pStepwiseGroup.Controls.Add(this.DownDelButton);
            this.pStepwiseGroup.Controls.Add(this.DownAutorotGridView);
            this.pStepwiseGroup.Controls.Add(this.upErrorLabel2);
            this.pStepwiseGroup.Controls.Add(this.upErrorLabel1);
            this.pStepwiseGroup.Controls.Add(this.UpAutorotGridView);
            this.pStepwiseGroup.Controls.Add(this.UpAddButton);
            this.pStepwiseGroup.Controls.Add(this.UpDelButton);
            this.pStepwiseGroup.Location = new System.Drawing.Point(4, 40);
            this.pStepwiseGroup.Name = "pStepwiseGroup";
            this.pStepwiseGroup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pStepwiseGroup.Size = new System.Drawing.Size(241, 166);
            this.pStepwiseGroup.TabIndex = 13;
            this.pStepwiseGroup.TabStop = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label43.Location = new System.Drawing.Point(123, 15);
            this.label43.Name = "label43";
            this.label43.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label43.Size = new System.Drawing.Size(47, 16);
            this.label43.TabIndex = 64;
            this.label43.Text = "Down";
            // 
            // downErrorLabel2
            // 
            this.downErrorLabel2.AutoSize = true;
            this.downErrorLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downErrorLabel2.ForeColor = System.Drawing.Color.Red;
            this.downErrorLabel2.Location = new System.Drawing.Point(177, 21);
            this.downErrorLabel2.Name = "downErrorLabel2";
            this.downErrorLabel2.Size = new System.Drawing.Size(38, 13);
            this.downErrorLabel2.TabIndex = 63;
            this.downErrorLabel2.Text = "value";
            // 
            // downErrorLabel1
            // 
            this.downErrorLabel1.AutoSize = true;
            this.downErrorLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downErrorLabel1.ForeColor = System.Drawing.Color.Red;
            this.downErrorLabel1.Location = new System.Drawing.Point(177, 9);
            this.downErrorLabel1.Name = "downErrorLabel1";
            this.downErrorLabel1.Size = new System.Drawing.Size(45, 13);
            this.downErrorLabel1.TabIndex = 62;
            this.downErrorLabel1.Text = "Invalid";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label32.Location = new System.Drawing.Point(7, 15);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(27, 16);
            this.label32.TabIndex = 61;
            this.label32.Text = "Up";
            // 
            // DownAddButton
            // 
            this.DownAddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DownAddButton.Location = new System.Drawing.Point(123, 36);
            this.DownAddButton.Name = "DownAddButton";
            this.DownAddButton.Size = new System.Drawing.Size(43, 22);
            this.DownAddButton.TabIndex = 59;
            this.DownAddButton.Text = "add";
            this.DownAddButton.UseVisualStyleBackColor = true;
            this.DownAddButton.Click += new System.EventHandler(this.DownAddButton_Click);
            // 
            // DownDelButton
            // 
            this.DownDelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DownDelButton.Location = new System.Drawing.Point(167, 36);
            this.DownDelButton.Name = "DownDelButton";
            this.DownDelButton.Size = new System.Drawing.Size(43, 22);
            this.DownDelButton.TabIndex = 60;
            this.DownDelButton.Text = "del";
            this.DownDelButton.UseVisualStyleBackColor = true;
            this.DownDelButton.Click += new System.EventHandler(this.DownDeleteButton_Click);
            // 
            // DownAutorotGridView
            // 
            this.DownAutorotGridView.AllowUserToAddRows = false;
            this.DownAutorotGridView.AllowUserToDeleteRows = false;
            this.DownAutorotGridView.AllowUserToResizeColumns = false;
            this.DownAutorotGridView.AllowUserToResizeRows = false;
            this.DownAutorotGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DownAutorotGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.DownAutorotGridView.Location = new System.Drawing.Point(124, 60);
            this.DownAutorotGridView.Name = "DownAutorotGridView";
            this.DownAutorotGridView.RowHeadersWidth = 51;
            this.DownAutorotGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DownAutorotGridView.Size = new System.Drawing.Size(108, 82);
            this.DownAutorotGridView.TabIndex = 58;
            this.DownAutorotGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DownAutorotGridView_CellValueChanged);
            this.DownAutorotGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DownAutorotGridView_RowsAdded);
            this.DownAutorotGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.DownAutorotGridView_RowsRemoved);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewTextBoxColumn4.Frozen = true;
            this.dataGridViewTextBoxColumn4.HeaderText = "act";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.ToolTipText = "Activation angle";
            this.dataGridViewTextBoxColumn4.Width = 30;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridViewTextBoxColumn5.Frozen = true;
            this.dataGridViewTextBoxColumn5.HeaderText = "deact";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.ToolTipText = "Deactivation angle (< act and > previous act)";
            this.dataGridViewTextBoxColumn5.Width = 30;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn6.Frozen = true;
            this.dataGridViewTextBoxColumn6.HeaderText = "rot";
            this.dataGridViewTextBoxColumn6.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.ToolTipText = "Rotation angle";
            this.dataGridViewTextBoxColumn6.Width = 30;
            // 
            // upErrorLabel2
            // 
            this.upErrorLabel2.AutoSize = true;
            this.upErrorLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upErrorLabel2.ForeColor = System.Drawing.Color.Red;
            this.upErrorLabel2.Location = new System.Drawing.Point(49, 21);
            this.upErrorLabel2.Name = "upErrorLabel2";
            this.upErrorLabel2.Size = new System.Drawing.Size(38, 13);
            this.upErrorLabel2.TabIndex = 57;
            this.upErrorLabel2.Text = "value";
            // 
            // upErrorLabel1
            // 
            this.upErrorLabel1.AutoSize = true;
            this.upErrorLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upErrorLabel1.ForeColor = System.Drawing.Color.Red;
            this.upErrorLabel1.Location = new System.Drawing.Point(49, 9);
            this.upErrorLabel1.Name = "upErrorLabel1";
            this.upErrorLabel1.Size = new System.Drawing.Size(45, 13);
            this.upErrorLabel1.TabIndex = 56;
            this.upErrorLabel1.Text = "Invalid";
            // 
            // UpAutorotGridView
            // 
            this.UpAutorotGridView.AllowUserToAddRows = false;
            this.UpAutorotGridView.AllowUserToDeleteRows = false;
            this.UpAutorotGridView.AllowUserToResizeColumns = false;
            this.UpAutorotGridView.AllowUserToResizeRows = false;
            this.UpAutorotGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UpAutorotGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.UpAutorotGridView.Location = new System.Drawing.Point(7, 60);
            this.UpAutorotGridView.Name = "UpAutorotGridView";
            this.UpAutorotGridView.RowHeadersWidth = 51;
            this.UpAutorotGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.UpAutorotGridView.Size = new System.Drawing.Size(108, 82);
            this.UpAutorotGridView.TabIndex = 39;
            this.UpAutorotGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.UpAutorotGridView_CellValueChanged);
            this.UpAutorotGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.UpAutorotGridView_RowsAdded);
            this.UpAutorotGridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.UpAutorotGridView_RowsRemoved);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "act";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.ToolTipText = "Activation angle";
            this.dataGridViewTextBoxColumn1.Width = 30;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "deact";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.ToolTipText = "Deactivation angle (< act and > previous act)";
            this.dataGridViewTextBoxColumn2.Width = 30;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTextBoxColumn3.Frozen = true;
            this.dataGridViewTextBoxColumn3.HeaderText = "rot";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.ToolTipText = "Rotation angle";
            this.dataGridViewTextBoxColumn3.Width = 30;
            // 
            // UpAddButton
            // 
            this.UpAddButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpAddButton.Location = new System.Drawing.Point(6, 36);
            this.UpAddButton.Name = "UpAddButton";
            this.UpAddButton.Size = new System.Drawing.Size(43, 22);
            this.UpAddButton.TabIndex = 40;
            this.UpAddButton.Text = "add";
            this.UpAddButton.UseVisualStyleBackColor = true;
            this.UpAddButton.Click += new System.EventHandler(this.UpAddButton_Click);
            // 
            // UpDelButton
            // 
            this.UpDelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpDelButton.Location = new System.Drawing.Point(50, 36);
            this.UpDelButton.Name = "UpDelButton";
            this.UpDelButton.Size = new System.Drawing.Size(43, 22);
            this.UpDelButton.TabIndex = 41;
            this.UpDelButton.Text = "del";
            this.UpDelButton.UseVisualStyleBackColor = true;
            this.UpDelButton.Click += new System.EventHandler(this.UpDeleteButton_Click);
            // 
            // pManualGroup
            // 
            this.pManualGroup.Controls.Add(this.label21);
            this.pManualGroup.Controls.Add(this.label27);
            this.pManualGroup.Controls.Add(this.pAccumReset);
            this.pManualGroup.Controls.Add(this.label19);
            this.pManualGroup.Controls.Add(this.upNUD);
            this.pManualGroup.Controls.Add(this.downNUD);
            this.pManualGroup.Controls.Add(this.label22);
            this.pManualGroup.Controls.Add(this.label28);
            this.pManualGroup.Controls.Add(this.pSnapRP);
            this.pManualGroup.Controls.Add(this.pAdditivRB);
            this.pManualGroup.Controls.Add(this.label31);
            this.pManualGroup.Controls.Add(this.SetUpButton);
            this.pManualGroup.Controls.Add(this.SetDownButton);
            this.pManualGroup.Controls.Add(this.DownLabel);
            this.pManualGroup.Controls.Add(this.UpLabel);
            this.pManualGroup.Controls.Add(this.label26);
            this.pManualGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pManualGroup.Location = new System.Drawing.Point(4, 3);
            this.pManualGroup.Name = "pManualGroup";
            this.pManualGroup.Size = new System.Drawing.Size(252, 138);
            this.pManualGroup.TabIndex = 39;
            this.pManualGroup.TabStop = false;
            this.pManualGroup.Text = "Manual Rotation";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(136, 52);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 15);
            this.label21.TabIndex = 52;
            this.label21.Text = "Tilt";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(129, 64);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 15);
            this.label27.TabIndex = 56;
            this.label27.Text = "down";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(7, 85);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 15);
            this.label19.TabIndex = 53;
            this.label19.Text = "Mode";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(80, 59);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(25, 13);
            this.label22.TabIndex = 20;
            this.label22.Text = "deg";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(9, 52);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(23, 15);
            this.label28.TabIndex = 51;
            this.label28.Text = "Tilt";
            // 
            // pSnapRP
            // 
            this.pSnapRP.AutoSize = true;
            this.pSnapRP.Checked = true;
            this.pSnapRP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pSnapRP.Location = new System.Drawing.Point(12, 101);
            this.pSnapRP.Name = "pSnapRP";
            this.pSnapRP.Size = new System.Drawing.Size(50, 17);
            this.pSnapRP.TabIndex = 1;
            this.pSnapRP.TabStop = true;
            this.pSnapRP.Text = "Snap";
            this.pSnapRP.UseVisualStyleBackColor = true;
            // 
            // pAdditivRB
            // 
            this.pAdditivRB.AutoSize = true;
            this.pAdditivRB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pAdditivRB.Location = new System.Drawing.Point(12, 116);
            this.pAdditivRB.Name = "pAdditivRB";
            this.pAdditivRB.Size = new System.Drawing.Size(58, 17);
            this.pAdditivRB.TabIndex = 12;
            this.pAdditivRB.Text = "Accum";
            this.pAdditivRB.UseVisualStyleBackColor = true;
            this.pAdditivRB.CheckedChanged += new System.EventHandler(this.pAdditivRB_CheckedChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(223, 59);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(25, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "deg";
            // 
            // DownLabel
            // 
            this.DownLabel.AutoSize = true;
            this.DownLabel.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DownLabel.Location = new System.Drawing.Point(134, 22);
            this.DownLabel.Margin = new System.Windows.Forms.Padding(0);
            this.DownLabel.Name = "DownLabel";
            this.DownLabel.Size = new System.Drawing.Size(36, 22);
            this.DownLabel.TabIndex = 39;
            this.DownLabel.Text = "D :";
            // 
            // UpLabel
            // 
            this.UpLabel.AutoSize = true;
            this.UpLabel.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.UpLabel.Location = new System.Drawing.Point(4, 21);
            this.UpLabel.Name = "UpLabel";
            this.UpLabel.Size = new System.Drawing.Size(37, 24);
            this.UpLabel.TabIndex = 38;
            this.UpLabel.Text = "U :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(11, 64);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(21, 15);
            this.label26.TabIndex = 55;
            this.label26.Text = "up";
            // 
            // loopTimer
            // 
            this.loopTimer.Interval = 20;
            this.loopTimer.Tick += new System.EventHandler(this.loopTimer_Tick);
            // 
            // _devicesStatusLabel
            // 
            this._devicesStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._devicesStatusLabel.AutoSize = true;
            this._devicesStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this._devicesStatusLabel.Location = new System.Drawing.Point(18, 831);
            this._devicesStatusLabel.Name = "_devicesStatusLabel";
            this._devicesStatusLabel.Size = new System.Drawing.Size(62, 13);
            this._devicesStatusLabel.TabIndex = 58;
            this._devicesStatusLabel.Text = "Joysticks: 0";
            // 
            // _devicesStatusImage
            // 
            this._devicesStatusImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._devicesStatusImage.Image = global::XRNeckSaferApp.Properties.Resources.red_circle;
            this._devicesStatusImage.InitialImage = null;
            this._devicesStatusImage.Location = new System.Drawing.Point(7, 833);
            this._devicesStatusImage.Name = "_devicesStatusImage";
            this._devicesStatusImage.Size = new System.Drawing.Size(10, 10);
            this._devicesStatusImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this._devicesStatusImage.TabIndex = 59;
            this._devicesStatusImage.TabStop = false;
            // 
            // numericUpDownMultRight
            // 
            this.numericUpDownMultRight.ActionPropertyDescription = null;
            this.numericUpDownMultRight.ActionPropertyId = "AutorotationRAmplifyBy";
            this.numericUpDownMultRight.ActionPropertyName = "R Amplify By";
            this.numericUpDownMultRight.ActionPropertyOrder = 3;
            this.numericUpDownMultRight.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownMultRight.DefaultValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownMultRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMultRight.GroupsComponent = this._groups;
            this.numericUpDownMultRight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMultRight.Location = new System.Drawing.Point(146, 57);
            this.numericUpDownMultRight.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownMultRight.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownMultRight.Name = "numericUpDownMultRight";
            actionPropertyGroupItem1.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem1.Tag = actionPropertyGroup5;
            this.numericUpDownMultRight.SelectedGroup = actionPropertyGroupItem1;
            this.numericUpDownMultRight.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownMultRight.TabIndex = 29;
            this.numericUpDownMultRight.ValueChanged += new System.EventHandler(this.numericUpDownMultRight_ValueChanged);
            // 
            // _groups
            // 
            actionPropertyGroup1.Name = "Reset Buttons";
            actionPropertyGroup1.Order = 0;
            actionPropertyGroup2.Name = "Manual Rotation Buttons";
            actionPropertyGroup2.Order = 1;
            actionPropertyGroup3.Name = "Hold Buttons";
            actionPropertyGroup3.Order = 2;
            actionPropertyGroup4.Name = "Change Manual Rotation Values";
            actionPropertyGroup4.Order = 3;
            actionPropertyGroup5.Name = "Change Smooth Autorotation Values";
            actionPropertyGroup5.Order = 4;
            this._groups.Groups = new XRNeckSafer.ActionPropertyGroup[] {
        actionPropertyGroup1,
        actionPropertyGroup2,
        actionPropertyGroup3,
        actionPropertyGroup4,
        actionPropertyGroup5};
            // 
            // numericUpDownMultLeft
            // 
            this.numericUpDownMultLeft.ActionPropertyDescription = null;
            this.numericUpDownMultLeft.ActionPropertyId = "AutorotationLAmplifyBy";
            this.numericUpDownMultLeft.ActionPropertyName = "L Amplify By";
            this.numericUpDownMultLeft.ActionPropertyOrder = 1;
            this.numericUpDownMultLeft.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownMultLeft.DefaultValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownMultLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMultLeft.GroupsComponent = this._groups;
            this.numericUpDownMultLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMultLeft.Location = new System.Drawing.Point(146, 31);
            this.numericUpDownMultLeft.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownMultLeft.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownMultLeft.Name = "numericUpDownMultLeft";
            actionPropertyGroupItem2.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem2.Tag = actionPropertyGroup5;
            this.numericUpDownMultLeft.SelectedGroup = actionPropertyGroupItem2;
            this.numericUpDownMultLeft.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownMultLeft.TabIndex = 28;
            this.numericUpDownMultLeft.ValueChanged += new System.EventHandler(this.numericUpDownMultLeft_ValueChanged);
            // 
            // numericUpDownStartRight
            // 
            this.numericUpDownStartRight.ActionPropertyDescription = null;
            this.numericUpDownStartRight.ActionPropertyId = "AutorotationRStartAt";
            this.numericUpDownStartRight.ActionPropertyName = "R Start At";
            this.numericUpDownStartRight.ActionPropertyOrder = 2;
            this.numericUpDownStartRight.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownStartRight.DefaultValue = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownStartRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownStartRight.GroupsComponent = this._groups;
            this.numericUpDownStartRight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStartRight.Location = new System.Drawing.Point(47, 57);
            this.numericUpDownStartRight.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownStartRight.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownStartRight.Name = "numericUpDownStartRight";
            actionPropertyGroupItem3.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem3.Tag = actionPropertyGroup5;
            this.numericUpDownStartRight.SelectedGroup = actionPropertyGroupItem3;
            this.numericUpDownStartRight.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownStartRight.TabIndex = 27;
            this.numericUpDownStartRight.ValueChanged += new System.EventHandler(this.numericUpDownStartRight_ValueChanged);
            // 
            // numericUpDownStartLeft
            // 
            this.numericUpDownStartLeft.ActionPropertyDescription = null;
            this.numericUpDownStartLeft.ActionPropertyId = "AutorotationLStartAt";
            this.numericUpDownStartLeft.ActionPropertyName = "L Start At";
            this.numericUpDownStartLeft.ActionPropertyOrder = 0;
            this.numericUpDownStartLeft.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownStartLeft.DefaultValue = new decimal(new int[] {
            95,
            0,
            0,
            0});
            this.numericUpDownStartLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownStartLeft.GroupsComponent = this._groups;
            this.numericUpDownStartLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStartLeft.Location = new System.Drawing.Point(47, 31);
            this.numericUpDownStartLeft.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownStartLeft.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownStartLeft.Name = "numericUpDownStartLeft";
            actionPropertyGroupItem4.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem4.Tag = actionPropertyGroup5;
            this.numericUpDownStartLeft.SelectedGroup = actionPropertyGroupItem4;
            this.numericUpDownStartLeft.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownStartLeft.TabIndex = 26;
            this.numericUpDownStartLeft.ValueChanged += new System.EventHandler(this.numericUpDownStartLeft_ValueChanged);
            // 
            // YawAutorotationHoldButton
            // 
            this.YawAutorotationHoldButton.ActionPropertyDescription = null;
            this.YawAutorotationHoldButton.ActionPropertyId = "YawAutorotationHold";
            this.YawAutorotationHoldButton.ActionPropertyName = "Yaw";
            this.YawAutorotationHoldButton.ActionPropertyOrder = 0;
            this.YawAutorotationHoldButton.ActiveBackColour = System.Drawing.Color.Black;
            this.YawAutorotationHoldButton.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.YawAutorotationHoldButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.YawAutorotationHoldButton.GroupsComponent = this._groups;
            this.YawAutorotationHoldButton.InActiveBackColour = System.Drawing.Color.Empty;
            this.YawAutorotationHoldButton.InActiveForeColour = System.Drawing.Color.Empty;
            this.YawAutorotationHoldButton.Location = new System.Drawing.Point(196, 15);
            this.YawAutorotationHoldButton.Name = "YawAutorotationHoldButton";
            actionPropertyGroupItem12.Name = "Hold Buttons";
            actionPropertyGroupItem12.Tag = actionPropertyGroup3;
            this.YawAutorotationHoldButton.SelectedGroup = actionPropertyGroupItem12;
            this.YawAutorotationHoldButton.Size = new System.Drawing.Size(48, 22);
            this.YawAutorotationHoldButton.TabIndex = 42;
            this.YawAutorotationHoldButton.Text = "Hold";
            this.YawAutorotationHoldButton.UseVisualStyleBackColor = true;
            this.YawAutorotationHoldButton.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // AccumReset
            // 
            this.AccumReset.ActionPropertyDescription = "Resets the accumulated yaw angle";
            this.AccumReset.ActionPropertyId = "SetAccumResetYaw";
            this.AccumReset.ActionPropertyName = "Accum Yaw";
            this.AccumReset.ActionPropertyOrder = 1;
            this.AccumReset.ActiveBackColour = System.Drawing.Color.Black;
            this.AccumReset.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.AccumReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AccumReset.GroupsComponent = this._groups;
            this.AccumReset.InActiveBackColour = System.Drawing.Color.Empty;
            this.AccumReset.InActiveForeColour = System.Drawing.Color.Empty;
            this.AccumReset.Location = new System.Drawing.Point(69, 99);
            this.AccumReset.Name = "AccumReset";
            actionPropertyGroupItem6.Name = "Reset Buttons";
            actionPropertyGroupItem6.Tag = actionPropertyGroup1;
            this.AccumReset.SelectedGroup = actionPropertyGroupItem6;
            this.AccumReset.Size = new System.Drawing.Size(57, 34);
            this.AccumReset.TabIndex = 54;
            this.AccumReset.Text = "Set Acc. Reset";
            this.AccumReset.UseVisualStyleBackColor = true;
            this.AccumReset.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // transFNUP
            // 
            this.transFNUP.ActionPropertyDescription = null;
            this.transFNUP.ActionPropertyId = "TransitionOffsetForward";
            this.transFNUP.ActionPropertyName = "Translation Fwd";
            this.transFNUP.ActionPropertyOrder = 2;
            this.transFNUP.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.transFNUP.DefaultValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.transFNUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transFNUP.GroupsComponent = this._groups;
            this.transFNUP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.transFNUP.Location = new System.Drawing.Point(176, 87);
            this.transFNUP.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.transFNUP.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.transFNUP.Name = "transFNUP";
            actionPropertyGroupItem7.Name = "Manual Rotation Values";
            actionPropertyGroupItem7.Tag = actionPropertyGroup4;
            this.transFNUP.SelectedGroup = actionPropertyGroupItem7;
            this.transFNUP.Size = new System.Drawing.Size(44, 20);
            this.transFNUP.TabIndex = 26;
            this.transFNUP.ValueChanged += new System.EventHandler(this.OnYawForwardTranslationChanged);
            // 
            // angleNUD
            // 
            this.angleNUD.ActionPropertyDescription = null;
            this.angleNUD.ActionPropertyId = "YawRotationLR";
            this.angleNUD.ActionPropertyName = "Yaw Rotation L/R";
            this.angleNUD.ActionPropertyOrder = 0;
            this.angleNUD.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.angleNUD.DefaultValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.angleNUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.angleNUD.GroupsComponent = this._groups;
            this.angleNUD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.angleNUD.Location = new System.Drawing.Point(34, 63);
            this.angleNUD.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.angleNUD.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.angleNUD.Name = "angleNUD";
            actionPropertyGroupItem8.Name = "Manual Rotation Values";
            actionPropertyGroupItem8.Tag = actionPropertyGroup4;
            this.angleNUD.SelectedGroup = actionPropertyGroupItem8;
            this.angleNUD.Size = new System.Drawing.Size(38, 20);
            this.angleNUD.TabIndex = 9;
            // 
            // transLRNUP
            // 
            this.transLRNUP.ActionPropertyDescription = null;
            this.transLRNUP.ActionPropertyId = "TransitionOffsetLeftRight";
            this.transLRNUP.ActionPropertyName = "Translation L/R";
            this.transLRNUP.ActionPropertyOrder = 1;
            this.transLRNUP.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.transLRNUP.DefaultValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.transLRNUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transLRNUP.GroupsComponent = this._groups;
            this.transLRNUP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.transLRNUP.Location = new System.Drawing.Point(176, 63);
            this.transLRNUP.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.transLRNUP.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.transLRNUP.Name = "transLRNUP";
            actionPropertyGroupItem9.Name = "Manual Rotation Values";
            actionPropertyGroupItem9.Tag = actionPropertyGroup4;
            this.transLRNUP.SelectedGroup = actionPropertyGroupItem9;
            this.transLRNUP.Size = new System.Drawing.Size(44, 20);
            this.transLRNUP.TabIndex = 25;
            this.transLRNUP.ValueChanged += new System.EventHandler(this.OnYawLeftRightTranslationChanged);
            // 
            // SetLeftButton
            // 
            this.SetLeftButton.ActionPropertyDescription = null;
            this.SetLeftButton.ActionPropertyId = "ManualRotationLeft";
            this.SetLeftButton.ActionPropertyName = "Left";
            this.SetLeftButton.ActionPropertyOrder = 0;
            this.SetLeftButton.ActiveBackColour = System.Drawing.Color.Black;
            this.SetLeftButton.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.SetLeftButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetLeftButton.GroupsComponent = this._groups;
            this.SetLeftButton.InActiveBackColour = System.Drawing.SystemColors.Control;
            this.SetLeftButton.InActiveForeColour = System.Drawing.SystemColors.ControlText;
            this.SetLeftButton.Location = new System.Drawing.Point(39, 20);
            this.SetLeftButton.Name = "SetLeftButton";
            actionPropertyGroupItem10.Name = "Manual Rotation Buttons";
            actionPropertyGroupItem10.Tag = actionPropertyGroup2;
            this.SetLeftButton.SelectedGroup = actionPropertyGroupItem10;
            this.SetLeftButton.Size = new System.Drawing.Size(72, 27);
            this.SetLeftButton.TabIndex = 36;
            this.SetLeftButton.Text = "Set Button";
            this.SetLeftButton.UseVisualStyleBackColor = false;
            this.SetLeftButton.ActionPropertyValueChanged += new System.Action<bool>(this.OnSetLeftButtonActionPropertyValueChanged);
            this.SetLeftButton.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // SetRightButton
            // 
            this.SetRightButton.ActionPropertyDescription = null;
            this.SetRightButton.ActionPropertyId = "ManualRotationRight";
            this.SetRightButton.ActionPropertyName = "Right";
            this.SetRightButton.ActionPropertyOrder = 1;
            this.SetRightButton.ActiveBackColour = System.Drawing.Color.Black;
            this.SetRightButton.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.SetRightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetRightButton.GroupsComponent = this._groups;
            this.SetRightButton.InActiveBackColour = System.Drawing.SystemColors.Control;
            this.SetRightButton.InActiveForeColour = System.Drawing.SystemColors.ControlText;
            this.SetRightButton.Location = new System.Drawing.Point(167, 20);
            this.SetRightButton.Name = "SetRightButton";
            actionPropertyGroupItem11.Name = "Manual Rotation Buttons";
            actionPropertyGroupItem11.Tag = actionPropertyGroup2;
            this.SetRightButton.SelectedGroup = actionPropertyGroupItem11;
            this.SetRightButton.Size = new System.Drawing.Size(72, 27);
            this.SetRightButton.TabIndex = 37;
            this.SetRightButton.Text = "Set Button";
            this.SetRightButton.UseVisualStyleBackColor = false;
            this.SetRightButton.ActionPropertyValueChanged += new System.Action<bool>(this.OnSetRightButtonActionPropertyValueChanged);
            this.SetRightButton.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // numericUpDownMultDown
            // 
            this.numericUpDownMultDown.ActionPropertyDescription = null;
            this.numericUpDownMultDown.ActionPropertyId = "AutorotationDAmplifyBy";
            this.numericUpDownMultDown.ActionPropertyName = "D Amplify By";
            this.numericUpDownMultDown.ActionPropertyOrder = 7;
            this.numericUpDownMultDown.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownMultDown.DefaultValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownMultDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMultDown.GroupsComponent = this._groups;
            this.numericUpDownMultDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMultDown.Location = new System.Drawing.Point(146, 57);
            this.numericUpDownMultDown.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownMultDown.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownMultDown.Name = "numericUpDownMultDown";
            actionPropertyGroupItem13.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem13.Tag = actionPropertyGroup5;
            this.numericUpDownMultDown.SelectedGroup = actionPropertyGroupItem13;
            this.numericUpDownMultDown.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownMultDown.TabIndex = 29;
            this.numericUpDownMultDown.ValueChanged += new System.EventHandler(this.numericUpDownMultDown_ValueChanged);
            // 
            // numericUpDownMultUp
            // 
            this.numericUpDownMultUp.ActionPropertyDescription = null;
            this.numericUpDownMultUp.ActionPropertyId = "AutorotationUAmplifyBy";
            this.numericUpDownMultUp.ActionPropertyName = "U Amplify By";
            this.numericUpDownMultUp.ActionPropertyOrder = 5;
            this.numericUpDownMultUp.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownMultUp.DefaultValue = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDownMultUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMultUp.GroupsComponent = this._groups;
            this.numericUpDownMultUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMultUp.Location = new System.Drawing.Point(146, 31);
            this.numericUpDownMultUp.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownMultUp.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownMultUp.Name = "numericUpDownMultUp";
            actionPropertyGroupItem14.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem14.Tag = actionPropertyGroup5;
            this.numericUpDownMultUp.SelectedGroup = actionPropertyGroupItem14;
            this.numericUpDownMultUp.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownMultUp.TabIndex = 28;
            this.numericUpDownMultUp.ValueChanged += new System.EventHandler(this.numericUpDownMultUp_ValueChanged);
            // 
            // numericUpDownStartDown
            // 
            this.numericUpDownStartDown.ActionPropertyDescription = null;
            this.numericUpDownStartDown.ActionPropertyId = "AutorotationDStartAt";
            this.numericUpDownStartDown.ActionPropertyName = "D Start At";
            this.numericUpDownStartDown.ActionPropertyOrder = 6;
            this.numericUpDownStartDown.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownStartDown.DefaultValue = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownStartDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownStartDown.GroupsComponent = this._groups;
            this.numericUpDownStartDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStartDown.Location = new System.Drawing.Point(47, 57);
            this.numericUpDownStartDown.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownStartDown.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownStartDown.Name = "numericUpDownStartDown";
            actionPropertyGroupItem15.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem15.Tag = actionPropertyGroup5;
            this.numericUpDownStartDown.SelectedGroup = actionPropertyGroupItem15;
            this.numericUpDownStartDown.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownStartDown.TabIndex = 27;
            this.numericUpDownStartDown.ValueChanged += new System.EventHandler(this.numericUpDownStartDown_ValueChanged);
            // 
            // numericUpDownStartUp
            // 
            this.numericUpDownStartUp.ActionPropertyDescription = null;
            this.numericUpDownStartUp.ActionPropertyId = "AutorotationUStartAt";
            this.numericUpDownStartUp.ActionPropertyName = "U Start At";
            this.numericUpDownStartUp.ActionPropertyOrder = 4;
            this.numericUpDownStartUp.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.numericUpDownStartUp.DefaultValue = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDownStartUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownStartUp.GroupsComponent = this._groups;
            this.numericUpDownStartUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownStartUp.Location = new System.Drawing.Point(47, 31);
            this.numericUpDownStartUp.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownStartUp.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownStartUp.Name = "numericUpDownStartUp";
            actionPropertyGroupItem16.Name = "Smooth Autorotation Values";
            actionPropertyGroupItem16.Tag = actionPropertyGroup5;
            this.numericUpDownStartUp.SelectedGroup = actionPropertyGroupItem16;
            this.numericUpDownStartUp.Size = new System.Drawing.Size(44, 20);
            this.numericUpDownStartUp.TabIndex = 26;
            this.numericUpDownStartUp.ValueChanged += new System.EventHandler(this.numericUpDownStartUp_ValueChanged);
            // 
            // PitchAutorotationHoldButton
            // 
            this.PitchAutorotationHoldButton.ActionPropertyDescription = null;
            this.PitchAutorotationHoldButton.ActionPropertyId = "PitchAutorotationHold";
            this.PitchAutorotationHoldButton.ActionPropertyName = "Pitch";
            this.PitchAutorotationHoldButton.ActionPropertyOrder = 1;
            this.PitchAutorotationHoldButton.ActiveBackColour = System.Drawing.Color.Black;
            this.PitchAutorotationHoldButton.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.PitchAutorotationHoldButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PitchAutorotationHoldButton.GroupsComponent = this._groups;
            this.PitchAutorotationHoldButton.InActiveBackColour = System.Drawing.Color.Empty;
            this.PitchAutorotationHoldButton.InActiveForeColour = System.Drawing.Color.Empty;
            this.PitchAutorotationHoldButton.Location = new System.Drawing.Point(196, 15);
            this.PitchAutorotationHoldButton.Name = "PitchAutorotationHoldButton";
            actionPropertyGroupItem17.Name = "Hold Buttons";
            actionPropertyGroupItem17.Tag = actionPropertyGroup3;
            this.PitchAutorotationHoldButton.SelectedGroup = actionPropertyGroupItem17;
            this.PitchAutorotationHoldButton.Size = new System.Drawing.Size(48, 22);
            this.PitchAutorotationHoldButton.TabIndex = 42;
            this.PitchAutorotationHoldButton.Text = "Hold";
            this.PitchAutorotationHoldButton.UseVisualStyleBackColor = true;
            this.PitchAutorotationHoldButton.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // pAccumReset
            // 
            this.pAccumReset.ActionPropertyDescription = null;
            this.pAccumReset.ActionPropertyId = "SetAccumResetPitch";
            this.pAccumReset.ActionPropertyName = "Accum Pitch";
            this.pAccumReset.ActionPropertyOrder = 2;
            this.pAccumReset.ActiveBackColour = System.Drawing.Color.Black;
            this.pAccumReset.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.pAccumReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pAccumReset.GroupsComponent = this._groups;
            this.pAccumReset.InActiveBackColour = System.Drawing.Color.Empty;
            this.pAccumReset.InActiveForeColour = System.Drawing.Color.Empty;
            this.pAccumReset.Location = new System.Drawing.Point(69, 99);
            this.pAccumReset.Name = "pAccumReset";
            actionPropertyGroupItem18.Name = "Reset Buttons";
            actionPropertyGroupItem18.Tag = actionPropertyGroup1;
            this.pAccumReset.SelectedGroup = actionPropertyGroupItem18;
            this.pAccumReset.Size = new System.Drawing.Size(57, 34);
            this.pAccumReset.TabIndex = 54;
            this.pAccumReset.Text = "Set Acc. Reset";
            this.pAccumReset.UseVisualStyleBackColor = true;
            this.pAccumReset.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // upNUD
            // 
            this.upNUD.ActionPropertyDescription = null;
            this.upNUD.ActionPropertyId = "ManualPitchUp";
            this.upNUD.ActionPropertyName = "Pitch Up";
            this.upNUD.ActionPropertyOrder = 3;
            this.upNUD.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.upNUD.DefaultValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.upNUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upNUD.GroupsComponent = this._groups;
            this.upNUD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.upNUD.Location = new System.Drawing.Point(42, 56);
            this.upNUD.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.upNUD.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.upNUD.Name = "upNUD";
            actionPropertyGroupItem19.Name = "Manual Rotation Values";
            actionPropertyGroupItem19.Tag = actionPropertyGroup4;
            this.upNUD.SelectedGroup = actionPropertyGroupItem19;
            this.upNUD.Size = new System.Drawing.Size(38, 20);
            this.upNUD.TabIndex = 9;
            // 
            // downNUD
            // 
            this.downNUD.ActionPropertyDescription = null;
            this.downNUD.ActionPropertyId = "ManualPitchDown";
            this.downNUD.ActionPropertyName = "Pitch Down";
            this.downNUD.ActionPropertyOrder = 4;
            this.downNUD.ContextMenuStrip = this._numericUpDownMenuStrip;
            this.downNUD.DefaultValue = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.downNUD.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.downNUD.GroupsComponent = this._groups;
            this.downNUD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.downNUD.Location = new System.Drawing.Point(173, 56);
            this.downNUD.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.downNUD.Minimum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.downNUD.Name = "downNUD";
            actionPropertyGroupItem20.Name = "Manual Rotation Values";
            actionPropertyGroupItem20.Tag = actionPropertyGroup4;
            this.downNUD.SelectedGroup = actionPropertyGroupItem20;
            this.downNUD.Size = new System.Drawing.Size(44, 20);
            this.downNUD.TabIndex = 25;
            // 
            // SetUpButton
            // 
            this.SetUpButton.ActionPropertyDescription = null;
            this.SetUpButton.ActionPropertyId = "ManualRotationUp";
            this.SetUpButton.ActionPropertyName = "Up";
            this.SetUpButton.ActionPropertyOrder = 2;
            this.SetUpButton.ActiveBackColour = System.Drawing.Color.Black;
            this.SetUpButton.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.SetUpButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetUpButton.GroupsComponent = this._groups;
            this.SetUpButton.InActiveBackColour = System.Drawing.SystemColors.Control;
            this.SetUpButton.InActiveForeColour = System.Drawing.SystemColors.ControlText;
            this.SetUpButton.Location = new System.Drawing.Point(39, 20);
            this.SetUpButton.Name = "SetUpButton";
            actionPropertyGroupItem21.Name = "Manual Rotation Buttons";
            actionPropertyGroupItem21.Tag = actionPropertyGroup2;
            this.SetUpButton.SelectedGroup = actionPropertyGroupItem21;
            this.SetUpButton.Size = new System.Drawing.Size(72, 27);
            this.SetUpButton.TabIndex = 36;
            this.SetUpButton.Text = "Set Button";
            this.SetUpButton.UseVisualStyleBackColor = false;
            this.SetUpButton.ActionPropertyValueChanged += new System.Action<bool>(this.OnSetUpButtonActionPropertyValueChanged);
            this.SetUpButton.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // SetDownButton
            // 
            this.SetDownButton.ActionPropertyDescription = null;
            this.SetDownButton.ActionPropertyId = "ManualRotationDown";
            this.SetDownButton.ActionPropertyName = "Down";
            this.SetDownButton.ActionPropertyOrder = 3;
            this.SetDownButton.ActiveBackColour = System.Drawing.Color.Black;
            this.SetDownButton.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.SetDownButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetDownButton.GroupsComponent = this._groups;
            this.SetDownButton.InActiveBackColour = System.Drawing.SystemColors.Control;
            this.SetDownButton.InActiveForeColour = System.Drawing.SystemColors.ControlText;
            this.SetDownButton.Location = new System.Drawing.Point(167, 20);
            this.SetDownButton.Name = "SetDownButton";
            actionPropertyGroupItem22.Name = "Manual Rotation Buttons";
            actionPropertyGroupItem22.Tag = actionPropertyGroup2;
            this.SetDownButton.SelectedGroup = actionPropertyGroupItem22;
            this.SetDownButton.Size = new System.Drawing.Size(72, 27);
            this.SetDownButton.TabIndex = 37;
            this.SetDownButton.Text = "Set Button";
            this.SetDownButton.UseVisualStyleBackColor = false;
            this.SetDownButton.ActionPropertyValueChanged += new System.Action<bool>(this.OnSetDownButtonActionPropertyValueChanged);
            this.SetDownButton.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // SetResetButton
            // 
            this.SetResetButton.ActionPropertyDescription = null;
            this.SetResetButton.ActionPropertyId = "SetResetCenter";
            this.SetResetButton.ActionPropertyName = "Center";
            this.SetResetButton.ActionPropertyOrder = 0;
            this.SetResetButton.ActiveBackColour = System.Drawing.SystemColors.ControlText;
            this.SetResetButton.ActiveForeColour = System.Drawing.Color.LightGreen;
            this.SetResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SetResetButton.GroupsComponent = this._groups;
            this.SetResetButton.InActiveBackColour = System.Drawing.Color.Empty;
            this.SetResetButton.InActiveForeColour = System.Drawing.Color.Empty;
            this.SetResetButton.Location = new System.Drawing.Point(179, 50);
            this.SetResetButton.Name = "SetResetButton";
            actionPropertyGroupItem5.Name = "Reset Buttons";
            actionPropertyGroupItem5.Tag = actionPropertyGroup1;
            this.SetResetButton.SelectedGroup = actionPropertyGroupItem5;
            this.SetResetButton.Size = new System.Drawing.Size(71, 43);
            this.SetResetButton.TabIndex = 28;
            this.SetResetButton.Text = "Set Center Button";
            this.SetResetButton.UseVisualStyleBackColor = true;
            this.SetResetButton.Click += new System.EventHandler(this.OnBooleanActionButtonClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 850);
            this.Controls.Add(this._devicesStatusImage);
            this.Controls.Add(this._devicesStatusLabel);
            this.Controls.Add(this.YawPitchTab);
            this.Controls.Add(this.HMDYawBox);
            this.Controls.Add(this.VersionLabel);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.MaximumSize = new System.Drawing.Size(292, 2837);
            this.MinimumSize = new System.Drawing.Size(292, 370);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "XRNS (0 deg)";
            this.Load += new System.EventHandler(this.OnFormLoaded);
            this.Shown += new System.EventHandler(this.OnFormShown);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.stepwiseGroup.ResumeLayout(false);
            this.stepwiseGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AutorotGridView)).EndInit();
            this.linearGroup.ResumeLayout(false);
            this.linearGroup.PerformLayout();
            this._numericUpDownMenuStrip.ResumeLayout(false);
            this.HMDYawBox.ResumeLayout(false);
            this.HMDYawBox.PerformLayout();
            this.ManualGroup.ResumeLayout(false);
            this.ManualGroup.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ARGroup.ResumeLayout(false);
            this.ARGroup.PerformLayout();
            this.YawPitchTab.ResumeLayout(false);
            this.YawTab.ResumeLayout(false);
            this.PitchTab.ResumeLayout(false);
            this.pARGroup.ResumeLayout(false);
            this.pARGroup.PerformLayout();
            this.pLinearGroup.ResumeLayout(false);
            this.pLinearGroup.PerformLayout();
            this.pStepwiseGroup.ResumeLayout(false);
            this.pStepwiseGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DownAutorotGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpAutorotGridView)).EndInit();
            this.pManualGroup.ResumeLayout(false);
            this.pManualGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._devicesStatusImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transFNUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.angleNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transLRNUP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMultUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.downNUD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox stepwiseGroup;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox HMDYawBox;
        private System.Windows.Forms.Label HMDYawLabel;
        private System.Windows.Forms.Label VersionLabel;
        private System.Windows.Forms.GroupBox ManualGroup;
        private System.Windows.Forms.Label RightLabel;
        private System.Windows.Forms.Label LeftLabel;
        private XRNeckSafer.NumericActionUpDown transFNUP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private NumericActionUpDown angleNUD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private BooleanActionButton SetLeftButton;
        private System.Windows.Forms.RadioButton additivRB;
        private BooleanActionButton SetRightButton;
        private System.Windows.Forms.RadioButton snapRB;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView AutorotGridView;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.ToolTip toolTip1;
        private BooleanActionButton YawAutorotationHoldButton;
        private BooleanActionButton SetResetButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem advancedConfigToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startMinimzedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizeToTrayToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem resetOptionsToDefaultToolStripMenuItem;
        private System.Windows.Forms.Label error_label;
        private System.Windows.Forms.Button graphButton;
        private System.Windows.Forms.Label error_label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem PitchLimToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.DataGridViewTextBoxColumn act;
        private System.Windows.Forms.DataGridViewTextBoxColumn deact;
        private System.Windows.Forms.DataGridViewTextBoxColumn rot;
        private System.Windows.Forms.DataGridViewTextBoxColumn LR;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fwd;
        private XRNeckSafer.BooleanActionButton AccumReset;
        private System.Windows.Forms.ToolStripMenuItem aPILayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disableAPILayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listApiToolStripMenuItem;
        private System.Windows.Forms.GroupBox ARGroup;
        private System.Windows.Forms.RadioButton ARlinear;
        private System.Windows.Forms.RadioButton ARstepwise;
        private System.Windows.Forms.RadioButton AROffButton;
        private System.Windows.Forms.GroupBox linearGroup;
        private System.Windows.Forms.Label label2;
        private NumericActionUpDown numericUpDownMultRight;
        private NumericActionUpDown numericUpDownMultLeft;
        private NumericActionUpDown numericUpDownStartRight;
        private NumericActionUpDown numericUpDownStartLeft;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabControl YawPitchTab;
        private System.Windows.Forms.TabPage YawTab;
        private System.Windows.Forms.TabPage PitchTab;
        private System.Windows.Forms.GroupBox pManualGroup;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label27;
        private BooleanActionButton pAccumReset;
        private System.Windows.Forms.Label label19;
        private NumericActionUpDown upNUD;
        private NumericActionUpDown downNUD;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.RadioButton pSnapRP;
        private System.Windows.Forms.RadioButton pAdditivRB;
        private System.Windows.Forms.Label label31;
        private BooleanActionButton SetUpButton;
        private BooleanActionButton SetDownButton;
        private System.Windows.Forms.Label DownLabel;
        private System.Windows.Forms.Label UpLabel;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox pARGroup;
        private System.Windows.Forms.GroupBox pLinearGroup;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private NumericActionUpDown numericUpDownMultDown;
        private NumericActionUpDown numericUpDownMultUp;
        private NumericActionUpDown numericUpDownStartDown;
        private NumericActionUpDown numericUpDownStartUp;
        private System.Windows.Forms.RadioButton pARlinear;
        private System.Windows.Forms.RadioButton pARstepwise;
        private System.Windows.Forms.RadioButton pAROffButton;
        private System.Windows.Forms.GroupBox pStepwiseGroup;
        private System.Windows.Forms.Label upErrorLabel2;
        private System.Windows.Forms.Label upErrorLabel1;
        private System.Windows.Forms.DataGridView UpAutorotGridView;
        private System.Windows.Forms.Button UpAddButton;
        private System.Windows.Forms.Button UpDelButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button DownAddButton;
        private System.Windows.Forms.Button DownDelButton;
        private System.Windows.Forms.DataGridView DownAutorotGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label downErrorLabel2;
        private System.Windows.Forms.Label downErrorLabel1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ToolStripMenuItem disableAllGUIOutputToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.ComponentModel.IContainer components;
        private NumericActionUpDown transLRNUP;
        private System.Windows.Forms.Timer loopTimer;
        private BooleanActionButton PitchAutorotationHoldButton;
        private System.Windows.Forms.Label _devicesStatusLabel;
        private System.Windows.Forms.PictureBox _devicesStatusImage;
        private System.Windows.Forms.ToolStripMenuItem showJoystickkeyConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem _disableSplashScreenToolStripMenuItem;
        private ActionPropertyGroups _groups;
        private System.Windows.Forms.ContextMenuStrip _numericUpDownMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem setJoystickkeyShortcutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showJoystickkeyboardMappsToolStripMenuItem;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label42;
    }
}

