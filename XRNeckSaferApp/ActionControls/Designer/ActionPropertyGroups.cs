﻿using System.ComponentModel;

namespace XRNeckSafer
{
    public class ActionPropertyGroups: Component
    {
        public ActionPropertyGroup[] Groups { get; set; }
    }
}
