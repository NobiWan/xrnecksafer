﻿namespace XRNeckSafer
{
    public interface IActionPropertyGroups
    {
        ActionPropertyGroups GroupsComponent { get; set; }
        ActionPropertyGroupItem SelectedGroup { get; set; }
    }
}
