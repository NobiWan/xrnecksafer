﻿namespace XRNeckSafer
{
    public interface IActionPropertyName
    {
        string ActionPropertyId { get; set; }
    }
}
