﻿namespace XRNeckSafer.Wpf
{
    public class ActionPropertyDataModelChangeEventArgs
    {
        public ActionPropertyDataModel Model { get; set; }
        public string ChangedProperty { get; set; }
    }
}
